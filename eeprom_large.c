/*
 * @file: eeprom_large.c
 * @author: T. Carter 
 * @date: March 30, 2022
 *
 * This code is to provide methods to read and write 16-bit and 32-bit
 *	variables to the EEPROM of the PIC (it uses the built-in functions 
 *	for read_eeprom and write_eeprom)
 *
 */

#ifndef _STDINT
	#include <stdint.h>			// need this library from the CCS drivers folder
#endif

// The CCS built-in function read_eeprom will read and return one byte
//    from the EEPROM memory. The following two functions are added
//    to return a 16-bit and 32-bit unsigned int respectively.
// The macro __EEADDRESS__ is defined in the processor header file as
//    either an 8-bit or 16-bit variable depending on EEPROM size.
// For the 16-bit and 32-bit methods of read & write; the addr variable
//    should be the EEPROM address of the LSB (least-significant byte)
//    Thus, the EEPROM addresses for the 16-bit and 32-bit variables 
//    should be sequential or things can get really confusing

uint16_t read_eeprom16 (__EEADDRESS__ addr)
{
   uint8_t low, high;
   low = read_eeprom(addr++);       // read from eeprom at addr and then increment addr
   high = read_eeprom(addr);
   return(make16(high, low));
}

uint32_t read_eeprom32 (__EEADDRESS__ addr)
{
   uint8_t low, high;
   uint16_t wLow, wHigh;  
   low = read_eeprom(addr++);       // read from eeprom at addr and then increment addr
   high = read_eeprom(addr++);
   wLow = make16(high, low);
   low = read_eeprom(addr++);
   high = read_eeprom(addr);
   wHigh = make16(high, low);
   return(make32(wHigh, wLow));
}

// The following eeprom_write function only writes the data to EEPROM if
//    it is different from what is at that addr
void eeprom_write (__EEADDRESS__ addr, uint8_t data)
{
   uint8_t temp = read_eeprom(addr);
   if (data != temp)
   {
      write_eeprom(addr, data);
   }
}

// The following two functions are provided to write 16-bit and 32-bit
//    variables to the EEPROM; similar to the read_eeprom functions above
void write_eeprom16 (__EEADDRESS__ addr, uint16_t data)
{
   uint8_t smallData = make8(data, 0);
   eeprom_write(addr++, smallData);
   smallData = make8(data, 1);
   eeprom_write(addr, smallData);
}

void write_eeprom32 (__EEADDRESS__ addr, uint32_t data)
{
   uint8_t smallData = make8(data, 0);
   eeprom_write(addr++, smallData);
   smallData = make8(data, 1);
   eeprom_write(addr++, smallData);
   smallData = make8(data, 2);
   eeprom_write(addr++, smallData);
   smallData = make8(data, 3);
   eeprom_write(addr, smallData);
}
